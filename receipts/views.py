from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm
# Create your views here.


@login_required
def show_receipt(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt_list
    }
    return render(request, "receipts/list.html", context)


@login_required(redirect_field_name="login")
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form
    }
    return render(request, "receipts/create.html", context)


@login_required
def show_category(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": category_list
    }
    return render(request, "receipts/category_list.html", context)


@login_required
def show_account(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {
        "account_list": account_list
    }
    return render(request, "receipts/account_list.html", context)


@login_required(redirect_field_name="login")
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
    else:
        form = CategoryForm()

    context = {
        "form": form
    }
    return render(request, "receipts/category_create.html", context)


@login_required(redirect_field_name="login")
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form": form
    }
    return render(request, "receipts/account_create.html", context)

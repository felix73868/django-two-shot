from django.urls import path
from receipts.views import show_receipt, create_receipt
from receipts.views import show_category, create_category
from receipts.views import show_account, create_account

urlpatterns = [
    path("", show_receipt, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", show_category, name="category_list"),
    path("accounts/", show_account, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account")
]
